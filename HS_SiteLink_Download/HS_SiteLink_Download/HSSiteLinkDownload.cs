﻿using HS_SiteLink_Download.net.smdservers.api;
using HS_SiteLink_Download.net.smdservers.api1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using Timer = System.Timers.Timer;
using HelpfulClasses;

namespace HS_SiteLink_Download
{
    public partial class HSSiteLinkDownload : ServiceBase
    { 
        /// <summary>
        /// The Corporate Code. It should not change. It is used to connect to SiteLink.
        /// </summary>
        private const string corpCode = "C809";
        /// <summary>
        /// The Username. It should not change. It is used to connect to SiteLink.
        /// </summary>
        private const string username = "dbtest:::YOURSPACESTORAGE5N2R";
        /// <summary>
        /// The Password. It should not change. It is used to connect to SiteLink.
        /// </summary>
        private const string password = "pwdt4287";
        /// <summary>
        /// The collection of web services for SiteLink's Reporting API.
        /// </summary>
        private ReportingWs reportingAPI = new ReportingWs();
        /// <summary>
        /// The collection of web services for SiteLink's Transaction API.
        /// </summary>
        private CallCenterWs transactionAPI = new CallCenterWs();
        /// <summary>
        /// The timer that controls the interation of this service's code.
        /// </summary>
        private Timer timer = new Timer();
        /// <summary>
        /// The timer that controls the interation of this service's code.
        /// </summary>
        private Timer timerReceipts = new Timer();
        /// <summary>
        /// The list of the locations and related information.
        /// </summary>
        private List<LocationInformation> locations = new List<LocationInformation>();
        /// <summary>
        /// The list of properties.
        /// </summary>
        private List<Property> properties = new List<Property>();
        /// <summary>
        /// The start date for which to get the data for.
        /// </summary>
        private DateTime startDate = DateTime.MinValue;
        /// <summary>
        /// The end date for which to get the data for.
        /// </summary>
        private DateTime endDate = DateTime.MinValue;
        private SqlConnection sqlConnection;
        private OleDbConnection db2Connection;
        private SqlCommand sqlCommand;
        private OleDbCommand db2Command;
        private SqlTransaction sqlTransaction;
        private string strSQL;
        private int counter;
        private bool allowRun = false;
        private Dictionary<string, int> lastSiteReceipt = new Dictionary<string, int>();

        public HSSiteLinkDownload()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets the Management Summary data from SiteLink.
        /// </summary>
        /// <param name="location">The location code for which to get the data for.</param>
        /// <param name="startDate">The date from where to start getting the data.</param>
        /// <param name="endDate">The date from where to stop getting the data.</param>
        /// <returns>Return the a <seealso cref="DataSet"/> of the Management Summary data.</returns>
        private DataSet GetManagementSummary(string location, DateTime startDate, DateTime endDate)
        {
            try
            {
                DataSet locTbl;
                locTbl = reportingAPI.ManagementSummary(corpCode, location, username, password, startDate, endDate);
                return locTbl;
            }
            catch (Exception err)
            {
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                return null;
            }
        }

        /// <summary>
        /// Gets the Receipt data from SiteLink.
        /// </summary>
        /// <param name="location">The location code for which to get the data for.</param>
        /// <param name="startDate">The date from where to start getting the data.</param>
        /// <param name="endDate">The date from where to stop getting the data.</param>
        /// <returns>Return the a <seealso cref="DataSet"/> of the Receipts data.</returns>
        private DataSet GetReceipts(string location, DateTime startDate, DateTime endDate)
        {
            try
            {
                DataSet locTbl;
                locTbl = reportingAPI.Receipts(corpCode, location, username, password, startDate, endDate);
                return locTbl;
            }
            catch (Exception err)
            {
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                return null;
            }
        }

        /// <summary>
        /// Gets the Income Analysis data from SiteLink.
        /// </summary>
        /// <param name="location">The location code for which to get the data for.</param>
        /// <param name="startDate">The date from where to start getting the data.</param>
        /// <param name="endDate">The date from where to stop getting the data.</param>
        /// <returns>Return the a <seealso cref="DataSet"/> of the Income Analysis data.</returns>
        private DataSet GetIncomeAnalysis(string location, DateTime startDate, DateTime endDate)
        {
            try
            {
                DataSet locTbl;
                locTbl = reportingAPI.IncomeAnalysis(corpCode, location, username, password, startDate, endDate);
                return locTbl;
            }
            catch (Exception err)
            {
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                return null;
            }
        }

        /// <summary>
        /// Gets the General Journal data from SiteLink.
        /// </summary>
        /// <param name="location">The location code for which to get the data for.</param>
        /// <param name="startDate">The date from where to start getting the data.</param>
        /// <param name="endDate">The date from where to stop getting the data.</param>
        /// <returns>Return the a <seealso cref="DataSet"/> of the General Journal data.</returns>
        private DataSet GetGeneralJournal(string location, DateTime startDate, DateTime endDate)
        {
            try
            {
                DataSet locTbl;
                locTbl = reportingAPI.GeneralJournalEntries(corpCode, location, username, password, startDate, endDate);
                return locTbl;
            }
            catch (Exception err)
            {
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                return null;
            }
        }

        /// <summary>
        /// Creates a list of properties. They are hardcoded.
        /// </summary>
        private void CreateListOfProperties()
        {
            try
            {
                properties.Clear();

                Property property;

                try
                {
                    property = new Property(109, "L109", "BYMS", true, 50804);
                    properties.Add(property);
                }
                catch (Exception err)
                {
                    LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                }

                try
                {
                    property = new Property(87, "L087", "BAMS", true, 9967);
                    properties.Add(property);
                }
                catch (Exception err)
                {
                    LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                }

                try
                {
                    property = new Property(78, "L078", "OMMS", true, 5735);
                    properties.Add(property);
                }
                catch (Exception err)
                {
                    LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                }

                try
                {
                    property = new Property(77, "L077", "SCMS", true, 5);
                    properties.Add(property);
                }
                catch (Exception err)
                {
                    LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                }

                try
                {
                    property = new Property(73, "L073", "JMS", true, 1182);
                    properties.Add(property);
                }
                catch (Exception err)
                {
                    LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                }

                try
                {
                    property = new Property(71, "L071", "HMS", true, 1438);
                    properties.Add(property);
                }
                catch (Exception err)
                {
                    LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                }

                try
                {
                    property = new Property(68, "L068", "RRMS", true, 917);
                    properties.Add(property);
                }
                catch (Exception err)
                {
                    LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                }

                try
                {
                    property = new Property(67, "L067", "BCMS", true, 1615);
                    properties.Add(property);
                }
                catch (Exception err)
                {
                    LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                }

                try
                {
                    property = new Property(51, "L051", "CHMS", true, 45074);
                    properties.Add(property);
                }
                catch (Exception err)
                {
                    LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                }

                try
                {
                    property = new Property(49, "L049", "NMS", true, 35324);
                    properties.Add(property);
                }
                catch (Exception err)
                {
                    LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                }
            }
            catch (Exception err)
            {
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Logs a message to the Event Viewer on the system.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="type">The type of message to log.</param>
        public void LogMessage(string message, EventLogEntryType type)
        {
            //setup a new logger to write message to the "SLDownload" log
            EventLog log = new EventLog("SLDownload");
            log.Source = "SLDownload";

            //if the "SLDownload" log does not exist
            if (!EventLog.Exists("SLDownload"))
            {
                //create the log for "SLDownload"
                EventLog.CreateEventSource("SLDownload", "SLDownload");
            }

            //write the message and set the type
            log.WriteEntry(message.Trim(), type);
        }

        /// <summary>
        /// Writes the records for the all the reports from the data pulled in from SiteLink.
        /// </summary>
        /// <param name="locationInformation">A list of the Locations and their information from SiteLink.</param>
        /// <param name="startDate">The date where the data set begins.</param>
        /// <param name="endDate">The date where the data set ends.</param>
        private void WriteToReports(List<LocationInformation> locationInformation, DateTime startDate, DateTime endDate)
        {
            try
            {
                if(sqlConnection != null && (sqlConnection.State == ConnectionState.Open | sqlConnection.State == ConnectionState.Broken))
                {
                    sqlConnection.Close();
                    sqlConnection.Dispose();
                }

                if (db2Connection != null && (db2Connection.State == ConnectionState.Open | db2Connection.State == ConnectionState.Broken))
                {
                    db2Connection.Close();
                    db2Connection.Dispose();
                }

                //set up the db2 and MS SQL connection
                sqlConnection = new SqlConnection(Properties.Settings.Default.connMSSQL);
                db2Connection = new OleDbConnection(Properties.Settings.Default.connAS400);

                //open the connections
                sqlConnection.Open();
                sqlTransaction = sqlConnection.BeginTransaction();
                db2Connection.Open();

                //goes through each location and their inforamtion from SiteLink
                foreach (LocationInformation l in locationInformation)
                {
                    lastSiteReceipt.Clear();

                    if (!Properties.Settings.Default.ForNonReceipts)
                    {
                        strSQL = @"SELECT SITEID, MAX(IRECEIPTNU) as IRECEIPTNU FROM HS_SLRECEIPTS
                           GROUP BY SITEID";

                        DataSet dtMax = new DataSet();

                        //get the max receipt for each property
                        SqlDataAdapter da = new SqlDataAdapter(strSQL, Properties.Settings.Default.connMSSQL);
                        da.Fill(dtMax);
                        da.Dispose();

                        foreach (DataRow dr in dtMax.Tables[0].Rows)
                        {
                            lastSiteReceipt.Add(dr["SITEID"].ToString().Trim().PadLeft(3, '0'), int.Parse(dr["IRECEIPTNU"].ToString().Trim()));
                        }

                        WriteReceipts(startDate, endDate, l);
                    }
                    else
                    {
                        WriteIncomeAnalysis(startDate, endDate, l);
                        WriteGeneralLedgerEntries(startDate, endDate, l);
                        WriteManagementSummary(endDate, l);
                    }
                }

                sqlTransaction.Commit();

                //close and clear the connections
                sqlConnection.Close();
                sqlConnection.Dispose();
                db2Connection.Close();
                db2Connection.Dispose();
            }
            catch (SqlException err)
            {
                try
                {
                    LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                    sqlTransaction.Rollback();
                }
                catch (Exception e)
                {

                }

                try
                {
                    sqlConnection.Close();
                }
                catch (Exception e)
                {

                }
                try
                {
                    sqlConnection.Dispose();
                }
                catch (Exception e)
                {

                }
                try
                {
                    db2Connection.Close();
                }
                catch (Exception e)
                {

                }
                try
                {
                    db2Connection.Dispose();
                }
                catch (Exception e)
                {

                }

                sqlConnection = new SqlConnection(Properties.Settings.Default.connMSSQL);
                db2Connection = new OleDbConnection(Properties.Settings.Default.connAS400);
            }
            catch (Exception err)
            {
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                sqlTransaction.Rollback();
            }
        }

        /// <summary>
        /// Writes the MRI residential properties' arrears, vacancies, and moveouts for the current period, into the AS400.
        /// </summary>
        /// <param name="endDate">The end date for the current period.</param>
        private void WriteArrears(DateTime endDate)
        {
            try
            {
                //set up the db2 and MS SQL connection
                sqlConnection = new SqlConnection(Properties.Settings.Default.connMSSQL);
                db2Connection = new OleDbConnection(Properties.Settings.Default.connAS400);

                //open the connections
                sqlConnection.Open();
                db2Connection.Open();

                SqlDataReader sql;

                //get the residential properties' arrears, vacancies, and moveouts for the current period, ignoring WCC and 551
                strSQL = "SELECT * FROM HS_PROPARR " +
                         " WHERE RMPROPID NOT IN ('WCC', '551', '049', '051', '067', '068', '071', '073', '077', '078', '087', '109') AND PERIOD = @ENDDATE";

                sqlCommand = new SqlCommand(strSQL, sqlConnection, sqlTransaction);

                sqlCommand.Parameters.Add(new SqlParameter("@ENDDATE", endDate.ToString("yyyyMM")));

                sql = sqlCommand.ExecuteReader();

                sqlCommand.Dispose();

                //go through each property
                while (sql.Read())
                {
                    try
                    {
                        LogMessage(string.Format("Writting Arrears, Vacancies, and Moveouts for Residential property {0}.", sql["RMPROPID"].ToString().Trim()), EventLogEntryType.Information);
                        WritePropArrears(endDate, sql["RMPROPID"].ToString().Trim(), int.Parse(sql["VACANT"].ToString().Trim().PadRight(1, '0')), decimal.Parse(sql["ARREARS"].ToString().Trim().PadRight(1, '0')), int.Parse(sql["MOVEOUTS"].ToString().Trim().PadRight(1, '0')));
                    }
                    catch (Exception err)
                    {
                        LogMessage("The Arrears for property {0} were not written. The cause: \n " + err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                    }
                }

                //close and clear the connections
                sqlConnection.Close();
                sqlConnection.Dispose();
                db2Connection.Close();
                db2Connection.Dispose();
            }
            catch (SqlException err)
            {
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }
            catch (Exception err)
            {
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }
        }

        private void WriteManagementSummary(DateTime endDate, LocationInformation l)
        {
            try
            {
                //resets the counter before the next data set
                counter = 0;

                //if there is no data for the Management Summary, skip this section
                if (l.ManagementSummary != null)
                {
                    //gets the Management Summary
                    DataSet managementSummary = l.ManagementSummary;

                    try
                    {
                        OleDbDataReader sqlReader;
                        //gets the current year's record
                        strSQL = "SELECT * FROM MOVEOUTS" +
                                 " WHERE YEAR = " + endDate.Year + " AND PROP = " + l.Property.PropNo;

                        db2Command = new OleDbCommand(strSQL, db2Connection);

                        sqlReader = db2Command.ExecuteReader();

                        //if there is no record
                        if (!sqlReader.HasRows)
                        {
                            //inserts the current year's record and set each month's Move-Outs to 0
                            strSQL = "INSERT INTO MOVEOUTS(PROP, YEAR, OPEN3, JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC, AVG, TFACTR, OPEN15, DELETE) VALUES" +
                                     " (" + l.Property.PropNo + ", " + endDate.Year + ", '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '')";

                            db2Command = new OleDbCommand(strSQL, db2Connection);

                            db2Command.ExecuteNonQuery();

                            db2Command.Dispose();
                        }

                        sqlReader.Close();

                        //gets the UnitActivity data set and parse out the Move-Outs
                        DataRow tableUnitActivity = (from UnitActivity in managementSummary.Tables["UnitActivity"].AsEnumerable()
                                                     where UnitActivity.Field<string>("sDesc").ToString().Trim() == "Move-Outs"
                                                     select UnitActivity).Single<DataRow>();

                        //update to the past month's column for the current location
                        strSQL = "UPDATE MOVEOUTS" +
                                 " SET " + endDate.Date.ToString("MMM") + " = " + tableUnitActivity.Field<int>("iDCount").ToString().Trim() +
                                 " WHERE YEAR = '" + endDate.Year + "' AND PROP = " + l.Property.PropNo;

                        db2Command = new OleDbCommand(strSQL, db2Connection);

                        db2Command.ExecuteNonQuery();

                        db2Command.Dispose();
                    }
                    catch (Exception err)
                    {
                        LogMessage(string.Format("Moveouts for month {0} for property id {1} could not be updated because of an error.", endDate.Date.ToString("MMM"), l.Property.CorpCode), EventLogEntryType.Error);
                        LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                    }

                    SqlConnection sqlConnection2 = new SqlConnection(Properties.Settings.Default.connMSSQL);
                    SqlCommand sqlCommand2 = new SqlCommand();

                    try
                    {
                        //removes the vacant unit data for the past month and for the current location
                        strSQL = "DELETE FROM VACUNITS" +
                                     " WHERE YR = " + endDate.Year + " AND MO = " + endDate.Month + " AND PROP = " + l.Property.PropNo;

                        db2Command = new OleDbCommand(strSQL, db2Connection);

                        db2Command.ExecuteNonQuery();

                        db2Command.Dispose();

                        //gets the RentalActivity data set, and should only be one record
                        DataRow tableVacUnits = (from UnitActivity in managementSummary.Tables["RentalActivity"].AsEnumerable()
                                                 select UnitActivity).Single<DataRow>();

                        //inserts the data for the past month's vacant unit for the current location
                        strSQL = "INSERT INTO VACUNITS(RECID, YR, MO, PROP, VAC) VALUES" +
                                 " ('V', " + endDate.Year + ", " + endDate.Month + ", " +
                                 l.Property.PropNo + ", " + tableVacUnits.Field<int>("Vacant").ToString().Trim() + ")";

                        db2Command = new OleDbCommand(strSQL, db2Connection);

                        db2Command.ExecuteNonQuery();

                        db2Command.Dispose();

                        sqlConnection2 = new SqlConnection(Properties.Settings.Default.connMSSQL);

                        sqlConnection2.Open(); 

                        //inserts the data for the past month's vacant unit for the current location
                        strSQL = "INSERT INTO HS_VACUNITS(RECID, YR, MO, PROP, VAC) VALUES" +
                                 " ('V', " + endDate.Year + ", " + endDate.Month + ", " +
                                 l.Property.PropNo + ", " + tableVacUnits.Field<int>("Vacant").ToString().Trim() + ")";

                        sqlCommand2 = new SqlCommand(strSQL, sqlConnection2);

                        sqlCommand2.ExecuteNonQuery();

                        sqlConnection2.Close();
                        sqlCommand2.Dispose();
                    }
                    catch (Exception err)
                    {
                        LogMessage(string.Format("Vacant Units for month {0} for property id {1} could not be updated because of an error.", endDate.Date.ToString("MMM"), l.Property.CorpCode), EventLogEntryType.Error);
                        LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                        try
                        {
                            sqlConnection2.Close();
                            sqlCommand2.Dispose();
                        }
                        catch (Exception innerErr)
                        {

                        }
                    }

                    try
                    {
                        //removes the past month's Arreas for the property
                        strSQL = "DELETE FROM PROPARR" +
                                " WHERE YEAR = " + endDate.Year + " AND MONTH = " + endDate.Month + " AND PROP = " + l.Property.PropNo;

                        db2Command = new OleDbCommand(strSQL, db2Connection);

                        db2Command.ExecuteNonQuery();

                        db2Command.Dispose();

                        //gets all the past month's unpaid amounts for the location
                        List<DataRow> tableUnpaid = (from UnitActivity in managementSummary.Tables["Unpaid"].AsEnumerable()
                                                     select UnitActivity).ToList<DataRow>();

                        decimal unpaid = 0M;

                        //total the unpaid amounts
                        foreach (DataRow r in tableUnpaid)
                        {
                            unpaid += decimal.Parse(r["dcDlqntTot"].ToString().Trim());
                        }

                        //inserts the past month's unpaid amount
                        strSQL = "INSERT INTO PROPARR(YEAR, MONTH, PROP, ARRCUR) VALUES" +
                                 " (" + endDate.Year + ", " + endDate.Month + ", " + l.Property.PropNo + ", " + unpaid + ")";

                        db2Command = new OleDbCommand(strSQL, db2Connection);

                        db2Command.ExecuteNonQuery();

                        db2Command.Dispose();

                        sqlConnection2 = new SqlConnection(Properties.Settings.Default.connMSSQL);

                        sqlConnection2.Open();

                        //inserts the data for the past month's vacant unit for the current location
                        strSQL = "INSERT INTO HS_PROPARR(PERIOD, RMPROPID, ARREARS, VACANT, MOVEOUTS) VALUES " +
                                 " ('" + endDate.Period() + "', '" + l.Property.PropNo.ToString().PadLeft(3, '0') + "', " + unpaid + ", " +
                                 (from UnitActivity in managementSummary.Tables["RentalActivity"].AsEnumerable()
                                  select UnitActivity).Single<DataRow>().Field<int>("Vacant").ToString().Trim() + ", " +
                                  (from UnitActivity in managementSummary.Tables["UnitActivity"].AsEnumerable()
                                   where UnitActivity.Field<string>("sDesc").ToString().Trim() == "Move-Outs"
                                   select UnitActivity).Single<DataRow>().Field<int>("iDCount").ToString().Trim() + ")";

                        sqlCommand2 = new SqlCommand(strSQL, sqlConnection2);

                        sqlCommand2.ExecuteNonQuery();

                        sqlConnection2.Close();
                        sqlCommand2.Dispose();
                    }
                    catch (Exception err)
                    {
                        LogMessage(string.Format("Unpaid amount for month {0} for property id {1} could not be updated because of an error.", endDate.Date.ToString("MMM"), l.Property.CorpCode), EventLogEntryType.Error);
                        LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                        try
                        {
                            sqlConnection2.Close();
                            sqlCommand2.Dispose();
                        }
                        catch (Exception innerErr)
                        {

                        }
                    }
                }
            }
            catch (Exception err)
            {
                LogMessage(string.Format("Unpaid amount for property id {1} could not be updated because of an error.", endDate.Date.ToString("MMM"), l.Property.CorpCode), EventLogEntryType.Error);
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }
        }

        private void WriteGeneralLedgerEntries(DateTime startDate, DateTime endDate, LocationInformation l)
        {
            try
            {
                //resets the counter before the next data set
                counter = 0;

                //if there is no data for the General Journal, skip this section
                if (l.GeneralJournal != null)
                {
                    //gets the General Journal entries
                    DataSet generalJournal = l.GeneralJournal;
                    DataTable tableGeneralJournal = generalJournal.Tables[0];

                    LogMessage(string.Format("Deleting {3} General Journal Entries for property {0} from {1} through {2}.", l.Property.CorpCode.Trim(),
                        startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), tableGeneralJournal.Rows.Count), EventLogEntryType.Information);
                    //removes the past month's General Journal entries for the location
                    strSQL = "DELETE FROM HS_SLGENJOURN" +
                             " WHERE (DSTRT >= @STARTDATE AND DEND <= @ENDDATE) AND SITEID = @SITEID";

                    sqlCommand = new SqlCommand(strSQL, sqlConnection, sqlTransaction);

                    sqlCommand.Parameters.Add(new SqlParameter("@STARTDATE", startDate.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                    sqlCommand.Parameters.Add(new SqlParameter("@ENDDATE", endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                    sqlCommand.Parameters.Add(new SqlParameter("@SITEID", l.Property.PropNo));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();

                    LogMessage(string.Format("General Journal is being added for property id {0} for {1} to {2}.", l.Property.CorpCode.Trim(),
                        startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")), EventLogEntryType.Information);
                    //goes through the General Journal entries for location
                    foreach (DataRow d in tableGeneralJournal.Rows)
                    {
                        if (d["AcctCode"].ToString().Length < 6)
                        {
                            decimal Debit = decimal.Parse(d["Debit"].ToString().Trim());
                            decimal credit = decimal.Parse(d["Credit"].ToString().Trim());

                            if ((Debit != 0M) | (credit != 0M))
                            {
                                string Body = string.Format("Invalid GL acct for property", d["SiteID"].ToString().Trim(), d["AcctCode"].ToString().Trim());
                                string Subject = string.Format("{0}: Invalid GL Acct for  property {1} Acct : {2} {3} {4} {5}.", DateTime.Now, d["SiteID"].ToString().Trim(),
                                              d["AcctCode"].ToString().Trim(), d["Description"].ToString().Trim(), d["AcctName"].ToString().Trim(), d["SubAcctName"].ToString().Trim());

                                SendEmail(Body.Trim(), Subject);
                            }
                        }
                        try
                        {
                            //insert the General Journal entry
                            strSQL = "INSERT INTO HS_SLGENJOURN VALUES" +
                                     " (@SITEID, @DSTRT, @DEND, @TYPEID, @DESCRIPTN, @ACCTNAME, @SUBACCTNM, @ACCTTYPNM, @ACCTCODE, @DEBIT, @CREDIT, @SDEFACCTCD, @SDEFACCTTNM)";

                            sqlCommand = new SqlCommand(strSQL, sqlConnection, sqlTransaction);

                            sqlCommand.Parameters.Add(new SqlParameter("@SITEID", l.Property.PropNo));
                            sqlCommand.Parameters.Add(new SqlParameter("@DSTRT", d["dStrt"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@DEND", d["dEnd"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@TYPEID", d["TypeID"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@DESCRIPTN", d["Description"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@ACCTNAME", d["AcctName"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@SUBACCTNM", d["SubAcctName"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@ACCTTYPNM", d["AcctTypeName"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@ACCTCODE", d["AcctCode"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@DEBIT", d["Debit"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@CREDIT", d["Credit"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@SDEFACCTCD", d["sDefAcctCode"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@SDEFACCTTNM", d["sDefAcctName"].ToString().Trim()));

                            sqlCommand.ExecuteNonQuery();

                            sqlCommand.Dispose();
                        }
                        catch (Exception err)
                        {
                            sqlCommand.Dispose();
                            LogMessage(string.Format("General Journal account name {0} and sub account name {1} for property id {2} could not be added because of an error.", d[5].ToString().Trim(), d[6].ToString().Trim(), l.Property.CorpCode), EventLogEntryType.Error);
                            LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                LogMessage(string.Format("General Journal for property id {0} could not be added because of an error.", l.Property.PropNo), EventLogEntryType.Error);
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }
        }

        private void WriteIncomeAnalysis(DateTime startDate, DateTime endDate, LocationInformation l)
        {
            try
            {
                //reset the counter for the next data set
                counter = 0;

                //if there is no data for the Income Analysis, skip this section
                if (l.IncomeAnalysis != null)
                {
                    //gets the Income Analysis
                    DataSet incomeAnalysis = l.IncomeAnalysis;
                    DataTable tableIncomeAnalysis = incomeAnalysis.Tables[0];

                    LogMessage(string.Format("Deleting {3} Income Analysis Report for property {0} from {1} through {2}.", l.Property.CorpCode.Trim(),
                        startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), tableIncomeAnalysis.Rows.Count), EventLogEntryType.Information);
                    //removes the past month's Income Analysis for the location
                    strSQL = "DELETE FROM HS_SLINCANAL" +
                             " WHERE (DSTRT >= @STARTDATE AND DEND <= @ENDDATE) AND SITEID = @SITEID";

                    sqlCommand = new SqlCommand(strSQL, sqlConnection, sqlTransaction);

                    sqlCommand.Parameters.Add(new SqlParameter("@STARTDATE", startDate.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                    sqlCommand.Parameters.Add(new SqlParameter("@ENDDATE", endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")));
                    sqlCommand.Parameters.Add(new SqlParameter("@SITEID", l.Property.PropNo));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();

                    LogMessage(string.Format("Income Analysis is being added for property id {0} for {1} to {2}.", l.Property.CorpCode.Trim(),
                        startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")), EventLogEntryType.Information);
                    //goes through each piece of data related to Income Analysis
                    foreach (DataRow d in tableIncomeAnalysis.Rows)
                    {
                        try
                        {
                            //insert the record for the Income Analysis
                            strSQL = "INSERT INTO HS_SLINCANAL VALUES" +
                                     " (@SITEID, @IORDER, @SDESC, @DSUBCOL, @DCMAINCOL, @DCPERCENT, @DCPCTTOT, @DSTRT, @DEND)";

                            sqlCommand = new SqlCommand(strSQL, sqlConnection, sqlTransaction);

                            sqlCommand.Parameters.Add(new SqlParameter("@SITEID", l.Property.PropNo));
                            sqlCommand.Parameters.Add(new SqlParameter("@IORDER", d["iOrder"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@SDESC", d["sDesc"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@DSUBCOL", d["dcSubCol"].ToString().Trim().PadLeft(1, '0')));
                            sqlCommand.Parameters.Add(new SqlParameter("@DCMAINCOL", d["dcMainCol"].ToString().Trim().PadLeft(1, '0')));
                            sqlCommand.Parameters.Add(new SqlParameter("@DCPERCENT", d["dcPercent"].ToString().Trim().PadLeft(1, '0')));
                            sqlCommand.Parameters.Add(new SqlParameter("@DCPCTTOT", d["dcPctTot"].ToString().Trim().PadLeft(1, '0')));
                            sqlCommand.Parameters.Add(new SqlParameter("@DSTRT", startDate));
                            sqlCommand.Parameters.Add(new SqlParameter("@DEND", endDate));

                            sqlCommand.ExecuteNonQuery();

                            sqlCommand.Dispose();
                        }
                        catch (Exception err)
                        {
                            sqlCommand.Dispose();
                            LogMessage(string.Format("Income Analysis order number {0} for property id {1} could not be added because of an error.", d[1].ToString().Trim(), l.Property.CorpCode), EventLogEntryType.Error);
                            LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                        }
                    }

                    LogMessage(string.Format("{0} records retrieved for property {1}.", counter, l.Property.CorpCode), EventLogEntryType.Information);
                }
            }
            catch (Exception err)
            {
                LogMessage(string.Format("Income Analysis for property id {0} could not be added because of an error.", l.Property.PropNo), EventLogEntryType.Error);
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }
        }

        private void WriteReceipts(DateTime startDate, DateTime endDate, LocationInformation l)
        {
            try
            {
                //if there is no data for the Receipts, skip this section
                if (l.Receipts != null)
                {
                    //resets the counter for the next data set
                    counter = 0;

                    //gets the Receipts data
                    DataSet receipts = l.Receipts;
                    DataTable tableReceipts = receipts.Tables[0];

                    LogMessage(string.Format("Receipts removed for property id {0} for {1} to {2} to avoid duplication.", l.Property.CorpCode.Trim(),
                        startDate.ToString("yyyy-MM-dd 00:00:00"), endDate.ToString("yyyy-MM-dd 23:59:59")), EventLogEntryType.Information);
                    //remove the past month's receipts for the location
                    strSQL = "DELETE FROM HS_SLRECEIPTS" +
                             " WHERE (RECEIPTDAT BETWEEN @STARTDATE AND @ENDDATE) AND SITEID = @SITEID";

                    sqlCommand = new SqlCommand(strSQL, sqlConnection, sqlTransaction);

                    sqlCommand.Parameters.Add(new SqlParameter("@STARTDATE", startDate.ToString("yyyy-MM-dd 00:00:00")));
                    sqlCommand.Parameters.Add(new SqlParameter("@ENDDATE", endDate.ToString("yyyy-MM-dd 23:59:59")));
                    sqlCommand.Parameters.Add(new SqlParameter("@SITEID", l.Property.PropNo));

                    sqlCommand.ExecuteNonQuery();

                    sqlCommand.Dispose();

                    LogMessage(string.Format("Receipts being added for property id {0} for {1} to {2}.", l.Property.CorpCode.Trim(),
                        startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")), EventLogEntryType.Information);
                    //goes through each Receipt for the location
                    foreach (DataRow d in tableReceipts.Rows)
                    {
                        try
                        {
                            //insert a receipt for the location
                            strSQL = "INSERT INTO HS_SLRECEIPTS(SITEID, UNITTYPE, IRECEIPTNU, RECEIPTDAT, SUNITNAME, SBY, TENANT, TAXEXMPTN, PMT_CASH, PMT_CHECK, PMT_CREDIT, " +
                                     " PMT_ACH, PMT_CC, PMT_DEBIT, PMT_APLREF, SCATEGORY, RECEIPTAMT, CKORCHGNO, SCCTERMID, SPMTTYPDES, SPMTMEMO, TENANTID) VALUES" +
                                     " (@SITEID, @UNITTYPE, @IRECEIPTNUM, @RECEIPTDAT, @SUNITNAME, @SBY, @TENANT, @TAXEXMPTN, @PMT_CASH, @PMT_CHECK, @PMT_CREDIT, " +
                                     " @PMT_ACH, @PMT_CC, @PMT_DEBIT, @PMT_APLREF, @SCATEGORY, @RECEIPTAMT, @CKORCHGNO, @SCCTERMID, @SPMTTYPDES, @SPMTMEMO, @TENANTID)";

                            sqlCommand = new SqlCommand(strSQL, sqlConnection, sqlTransaction);

                            sqlCommand.Parameters.Add(new SqlParameter("@SITEID", l.Property.PropNo.ToString().PadLeft(3, '0')));
                            sqlCommand.Parameters.Add(new SqlParameter("@UNITTYPE", d["UnitType"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@IRECEIPTNUM", d["iReceiptNum"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@RECEIPTDAT", d["ReceiptDate"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@SUNITNAME", d["sUnitName"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@SBY", d["sBy"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@TENANT", d["Tenant"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@TAXEXMPTN", d["TaxExemptNum"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@PMT_CASH", d["PMT_CASh"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@PMT_CHECK", d["PMT_Check"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@PMT_CREDIT", d["PMT_Credit"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@PMT_ACH", d["PMT_ACH"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@PMT_CC", d["PMT_CreditCard"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@PMT_DEBIT", d["PMT_Debit"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@PMT_APLREF", d["PMT_AppliedRefund"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@SCATEGORY", d["sCategory"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@RECEIPTAMT", d["ReceiptAmount"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@CKORCHGNO", d["CkOrChgNo"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@SCCTERMID", d["sccTerminalID"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@SPMTTYPDES", d["sPmtTypeDesc"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@SPMTMEMO", d["sPaymentMemo"].ToString().Trim()));
                            sqlCommand.Parameters.Add(new SqlParameter("@TENANTID", d["TenantID"].ToString().Trim()));

                            sqlCommand.ExecuteNonQuery();

                            sqlCommand.Dispose();

                            counter++;
                        }
                        catch (Exception err)
                        {
                            sqlCommand.Dispose();
                            LogMessage(string.Format("Receipt number {0} for property id {1} could not be added because of an error.", d[2].ToString().Trim(), l.Property.CorpCode), EventLogEntryType.Error);
                            LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                        }
                    }

                    LogMessage(string.Format("{0} Receipts retrieved for property {1}.", counter, l.Property.CorpCode.Trim()), EventLogEntryType.Information);
                }
            }
            catch (Exception err)
            {
                LogMessage(string.Format("Receipts for property id {0} could not be added because of an error.", l.Property.PropNo), EventLogEntryType.Error);
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Inserts a property's Arrears, Vacancies, and Moveouts, from MRI, in the AS400.
        /// </summary>
        /// <param name="endTime">The end date for the current period.</param>
        /// <param name="property">The property id.</param>
        /// <param name="vacant">The number of vacant units.</param>
        /// <param name="arrears">The amount in Arrears.</param>
        /// <param name="moveOuts">The amount in moveouts.</param>
        private void WritePropArrears(DateTime endTime, string property, int vacant, decimal arrears, int moveOuts)
        {
            try
            {
                OleDbDataReader sqlReader;
                //gets the current year's record
                strSQL = "SELECT * FROM MOVEOUTS" +
                         " WHERE YEAR = " + endDate.Year + " AND PROP = " + property;

                db2Command = new OleDbCommand(strSQL, db2Connection);

                sqlReader = db2Command.ExecuteReader();

                //if there is no record
                if (!sqlReader.HasRows)
                {
                    //inserts the current year's record and set each month's Move-Outs to 0
                    strSQL = "INSERT INTO MOVEOUTS(PROP, YEAR, OPEN3, JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC, AVG, TFACTR, OPEN15, DELETE) VALUES" +
                             " (" + property + ", " + endDate.Year + ", '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '')";

                    db2Command = new OleDbCommand(strSQL, db2Connection);

                    db2Command.ExecuteNonQuery();

                    db2Command.Dispose();
                }

                sqlReader.Close();

                //update to the past month's column for the current location
                strSQL = "UPDATE MOVEOUTS" +
                         " SET " + endDate.Date.ToString("MMM") + " = " + moveOuts +
                         " WHERE YEAR = '" + endDate.Year + "' AND PROP = " + property;

                db2Command = new OleDbCommand(strSQL, db2Connection);

                db2Command.ExecuteNonQuery();

                db2Command.Dispose();
            }
            catch (Exception err)
            {
                LogMessage(string.Format("Moveouts for month {0} for property id {1} could not be updated because of an error.", endDate.Date.ToString("MMM"), property), EventLogEntryType.Error);
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }

            try
            {
                //removes the vacant unit data for the past month and for the current location
                strSQL = "DELETE FROM VACUNITS" +
                             " WHERE YR = " + endDate.Year + " AND MO = " + endDate.Month + " AND PROP = " + property;

                db2Command = new OleDbCommand(strSQL, db2Connection);

                db2Command.ExecuteNonQuery();

                db2Command.Dispose();

                //inserts the data for the past month's vacant unit for the current location
                strSQL = "INSERT INTO VACUNITS(RECID, YR, MO, PROP, VAC) VALUES" +
                         " ('V', " + endDate.Year + ", " + endDate.Month + ", " +
                         property + ", " + vacant + ")";

                db2Command = new OleDbCommand(strSQL, db2Connection);

                db2Command.ExecuteNonQuery();

                db2Command.Dispose();

                SqlConnection sqlConnection2 = new SqlConnection(Properties.Settings.Default.connMSSQL);
                SqlCommand sqlCommand2;

                sqlConnection2.Open();

                strSQL = "INSERT INTO HS_VACUNITS(RECID, YR, MO, PROP, VAC) VALUES" +
                         " ('V', " + endDate.Year + ", " + endDate.Month + ", " +
                         property + ", " + vacant + ")";

                sqlCommand2 = new SqlCommand(strSQL, sqlConnection2);

                sqlCommand2.ExecuteNonQuery();

                sqlCommand2.Dispose();

                sqlConnection2.Close();
                sqlCommand2.Dispose();
            }
            catch (Exception err)
            {
                LogMessage(string.Format("Vacant Units for month {0} for property id {1} could not be updated because of an error.", endDate.Date.ToString("MMM"), property), EventLogEntryType.Error);
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }

            try
            {
                //removes the past month's Arreas for the property
                strSQL = "DELETE FROM PROPARR" +
                        " WHERE YEAR = " + endDate.Year + " AND MONTH = " + endDate.Month + " AND PROP = " + property;

                db2Command = new OleDbCommand(strSQL, db2Connection);

                db2Command.ExecuteNonQuery();

                db2Command.Dispose();

                //inserts the past month's unpaid amount
                strSQL = "INSERT INTO PROPARR(YEAR, MONTH, PROP, ARRCUR) VALUES" +
                         " (" + endDate.Year + ", " + endDate.Month + ", " + property + ", " + arrears + ")";

                db2Command = new OleDbCommand(strSQL, db2Connection);

                db2Command.ExecuteNonQuery();

                db2Command.Dispose();

                //SqlConnection sqlConnection3 = new SqlConnection(Properties.Settings.Default.connMSSQL);
                //SqlCommand sqlCommand3;

                //sqlConnection3.Open();

                ////inserts the data for the past month's vacant unit for the current location
                //strSQL = "INSERT INTO HS_PROPARR(PERIOD, RMPROPID, ARREARS) VALUES " +
                //         " ('" + endDate.Period() + "', '" + property.PadLeft(3, '0') + "', " + arrears + " )";

                //sqlCommand3 = new SqlCommand(strSQL, sqlConnection3);

                //sqlCommand3.ExecuteNonQuery();

                //sqlCommand3.Dispose();

                //sqlConnection3.Close();
                //sqlCommand3.Dispose();
            }
            catch (Exception err)
            {
                LogMessage(string.Format("Unpaid amount for month {0} for property id {1} could not be updated because of an error.", endDate.Date.ToString("MMM"), property), EventLogEntryType.Error);
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }
        }

        private string ValidateDataSet(DataSet data)
        {
            if (data.Tables.Contains("RT"))
            {
                DataTable locDt = data.Tables["RT"];
                LogMessage(locDt.Rows[0]["Ret_Msg"].ToString().Trim(), EventLogEntryType.Error);
                Thread.Sleep(15000);
                return locDt.Rows[0]["Ret_Code"].ToString().Trim();
            }
            else
            {
                return "";
            }
        }

        private void ElapsedTime(object sender, ElapsedEventArgs e)
        {
            try
            {
                //stops the timer before doing anything else
                timer.Stop();

                //gets the day of the month
                int monthDays = DateTime.Now.AddMonths(-1).Day;
                //gets the days left for the month
                int daysLeft = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month) - DateTime.Now.AddMonths(-1).Day;
                //gets the first date of the previous month
                startDate = DateTime.Now.AddMonths(-1).AddDays(-monthDays + 1).Date;
                //gets the last date of the previous month
                endDate = DateTime.Now.AddMonths(-1).AddDays(daysLeft).Date.AddSeconds(((60 * 60) * 24) - 1);

                sqlConnection = new SqlConnection(Properties.Settings.Default.connMSSQL);

                //checks to to make sure the day is the fifth of the month and the time is 1 in the morning
                if ((DateTime.Now.Day == 5 & DateTime.Now.ToString("HH:mm") == "01:00") | allowRun)
                {
                    Thread.Sleep(60000);

                    sqlConnection.Close();

                    CreateListOfProperties();

                    LogMessage(string.Format("Income Analysis, General Journal, and Management Summary will be retrieved for date range {0} to {1}",
                        startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")), EventLogEntryType.Information);

                    LocationInformation location = new LocationInformation();
                    //DataSet receipts = new DataSet();
                    DataSet incomeAnalysis = new DataSet();
                    DataSet generalJournal = new DataSet();
                    DataSet managementSummary = new DataSet();
                    DataSet genJrn = new DataSet();

                    //goes through each property and gets the Receipts, Income Analysis, General Journal, and Management Summary for each
                    foreach (Property p in properties)
                    {
                        try
                        {
                            //LogMessage(string.Format("Getting Receipts for property {0} from {1} through {2}.", p.CorpCode.Trim(),
                            //    startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")), EventLogEntryType.Information);
                            //receipts = GetReceipts(p.CorpCode, startDate, endDate);
                            LogMessage(string.Format("Getting Income Analysis Report for property {0} from {1} through {2}.", p.CorpCode.Trim(),
                                startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")), EventLogEntryType.Information);
                            incomeAnalysis = GetIncomeAnalysis(p.CorpCode, startDate, endDate);
                            LogMessage(string.Format("Getting General Journal Entries for property {0} from {1} through {2}.", p.CorpCode.Trim(),
                                startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")), EventLogEntryType.Information);
                            generalJournal = GetGeneralJournal(p.CorpCode, startDate, endDate);
                            LogMessage(string.Format("Getting Management Summary for property {0} from {1} through {2}.", p.CorpCode.Trim(),
                                startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")), EventLogEntryType.Information);
                            managementSummary = GetManagementSummary(p.CorpCode, startDate, endDate);

                            genJrn.Merge(generalJournal);

                            //makes sure that that the is the no error from SiteLink in retrieving the Income Analysis
                            if (ValidateDataSet(incomeAnalysis) != "")
                            {
                                switch (incomeAnalysis.Tables[0].Rows[0]["Ret_Code"].ToString().Trim())
                                {
                                    //if the error code is 90, SiteLink is telling the program to try again in 15 seconds
                                    case "-90":
                                        Thread.Sleep(15000);
                                        LogMessage(string.Format("Getting receipts for property {0} from {1} through {2}.", p.CorpCode.Trim(),
                                        startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")), EventLogEntryType.Information);
                                        incomeAnalysis = GetIncomeAnalysis(p.CorpCode, startDate, endDate);
                                        break;
                                    default:
                                        //if there are other errors, clear the data set returned from SiteLink
                                        incomeAnalysis.Tables.Clear();
                                        break;
                                }
                            }

                            //makes sure that that the is the no error from SiteLink in retrieving the General Journal
                            if (ValidateDataSet(generalJournal) != "")
                            {
                                switch (generalJournal.Tables[0].Rows[0]["Ret_Code"].ToString().Trim())
                                {
                                    //if the error code is 90, SiteLink is telling the program to try again in 15 seconds
                                    case "-90":
                                        Thread.Sleep(15000);
                                        LogMessage(string.Format("Getting receipts for property {0} from {1} through {2}.", p.CorpCode.Trim(),
                                        startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")), EventLogEntryType.Information);
                                        generalJournal = GetGeneralJournal(p.CorpCode, startDate, endDate);
                                        break;
                                    default:
                                        //if there are other errors, clear the data set returned from SiteLink
                                        generalJournal.Tables.Clear();
                                        break;
                                }
                            }

                            //makes sure that that the is the no error from SiteLink in retrieving the Management Summary
                            if (ValidateDataSet(managementSummary) != "")
                            {
                                switch (managementSummary.Tables[0].Rows[0]["Ret_Code"].ToString().Trim())
                                {
                                    //if the error code is 90, SiteLink is telling the program to try again in 15 seconds
                                    case "-90":
                                        Thread.Sleep(15000);
                                        LogMessage(string.Format("Getting receipts for property {0} from {1} through {2}.", p.CorpCode.Trim(),
                                        startDate.ToString("yyyy-MM-dd HH:mm:ss.fff"), endDate.ToString("yyyy-MM-dd HH:mm:ss.fff")), EventLogEntryType.Information);
                                        managementSummary = GetManagementSummary(p.CorpCode, startDate, endDate);
                                        break;
                                    default:
                                        //if there are other errors, clear the data set returned from SiteLink
                                        managementSummary.Tables.Clear();
                                        break;
                                }
                            }

                            //adds the data from SiteLink to the property
                            location = new LocationInformation(p, null, incomeAnalysis, generalJournal, managementSummary);

                            //adds the property to list of the locations
                            locations.Add(location);
                        }
                        catch (Exception err)
                        {
                            LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                        }
                    }

                    if (HasMissingAccounts(genJrn.Tables[0]))
                    {
                        timer.Interval = 86400000;
                        timer.Start();
                        allowRun = true;
                        return;
                    }
                    else
                    {
                        allowRun = false;
                        timer.Interval = 10000;
                    }

                    //write the data to the reports
                    WriteToReports(locations, startDate, endDate);

                    LogMessage("Reports are generated.", EventLogEntryType.Information);

                    locations.Clear();
                }
                else if (DateTime.Now.Day == 19 & DateTime.Now.ToString("HH:mm") == "01:00")
                {
                    Thread.Sleep(60000);

                    LogMessage("Writting Arrears, Vacancies, and Moveouts for Residential properties.", EventLogEntryType.Information);
                    WriteArrears(endDate);
                    LogMessage("Successfully wrote Arrears, Vacancies, and Moveouts for Residential properties.", EventLogEntryType.Information);
                }

                //starts the timer
                timer.Start();
            }
            catch (Exception err)
            {
                //timer.Stop();
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }
        }

        private void ElapsedTimeReceipts(object sender, ElapsedEventArgs e)
        {
            try
            {
                //stops the timer before doing anything else
                timerReceipts.Stop();

                //gets the current date
                startDate = DateTime.Now.AddMinutes(-20);
                //gets the current date
                endDate = DateTime.Now;

                CreateListOfProperties();

                LogMessage(string.Format("Receipts will be retrieved for date range {0} to {1}",
                    startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")), EventLogEntryType.Information);

                LocationInformation location = new LocationInformation();
                DataSet receipts = new DataSet();

                //goes through each property and gets the Receipts, Income Analysis, General Journal, and Management Summary for each
                foreach (Property p in properties)
                {
                    try
                    {
                        LogMessage(string.Format("Getting Receipts for property {0} from {1} through {2}.", p.CorpCode.Trim(),
                            startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")), EventLogEntryType.Information);
                        receipts = GetReceipts(p.CorpCode, startDate, endDate);

                        //makes sure that that the is the no error from SiteLink in retrieving the Receipts
                        if (ValidateDataSet(receipts) != "")
                        {
                            switch (receipts.Tables[0].Rows[0]["Ret_Code"].ToString().Trim())
                            {
                                //if the error code is 90, SiteLink is telling the program to try again in 15 seconds
                                case "-90":
                                    Thread.Sleep(15000);
                                    LogMessage(string.Format("Getting receipts for property {0} from {1} through {2}.", p.CorpCode.Trim(),
                                    startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")), EventLogEntryType.Information);
                                    receipts = GetReceipts(p.CorpCode, startDate, endDate);
                                    break;
                                default:
                                    //if there are other errors, clear the data set returned from SiteLink
                                    receipts.Tables.Clear();
                                    break;
                            }
                        }

                        //adds the data from SiteLink to the property
                        location = new LocationInformation(p, receipts);

                        //adds the property to list of the locations
                        locations.Add(location);
                    }
                    catch (Exception err)
                    {
                        LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
                    }
                }

                //write the data to the reports
                WriteToReports(locations, startDate, endDate);

                LogMessage("Reports are generated.", EventLogEntryType.Information);

                locations.Clear();

                //starts the timer
                timerReceipts.Start();
            }
            catch (Exception err)
            {
                timerReceipts.Stop();
                LogMessage(err.Message.ToString().Trim() + " \n" + err.StackTrace.Trim(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Sends an email to the help desk when a error happens.
        /// </summary>
        /// <param name="Body">The body of the email.</param>
        /// <param name="Subject">The subject of the email.</param>
        private void SendEmail(string Body, string Subject)
        {
            try
            {
                List<MailAddress> emailTo = new List<MailAddress>();
                DataSet locTblEmails = new DataSet();

                //compare the totals of the Cash, Check, and Money Orders to the Cash Management Summary
                if (Debugger.IsAttached)
                {
                    SqlDataAdapter da = new SqlDataAdapter(@"SELECT HSVAL FROM HS_CODEVAL
                                                              WHERE HSTABLE = 'EMLGRP' AND HSCOL = 'ACTEMLADR' AND HSVAL <> '@@@@@'", Properties.Settings.Default.connMSSQL);
                    da.Fill(locTblEmails);

                    foreach (DataRow row in locTblEmails.Tables[0].Rows)
                    {
                        emailTo.Add(new MailAddress(row["HSVAL"].ToString().Trim()));
                    }

                    emailTo.Clear();

                    emailTo.Add(new MailAddress("jkelly@hillmgt.com"));
                    emailTo.Add(new MailAddress("jhall@hillmgt.com"));
                }
                else
                {//send to the accounting group
                    SqlDataAdapter da = new SqlDataAdapter(@"SELECT HSVAL FROM HS_CODEVAL
                                                              WHERE HSTABLE = 'EMLGRP' AND HSCOL = 'ACTEMLADR' AND HSVAL <> '@@@@@'", Properties.Settings.Default.connMSSQL);
                    da.Fill(locTblEmails);

                    foreach (DataRow row in locTblEmails.Tables[0].Rows)
                    {
                        emailTo.Add(new MailAddress(row["HSVAL"].ToString().Trim()));
                    }
                }

                string recipients = "";

                foreach (MailAddress email in emailTo)
                {
                    recipients += email.Address.Trim() + ";";
                }

                sqlConnection = new SqlConnection(Properties.Settings.Default.connMSSQL);

                if(sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand = new SqlCommand($@"EXEC msdb.dbo.sp_send_dbmail
	                                                    @profile_name = N'Notifications',
	                                                    @recipients = N'{recipients.Trim()}',
	                                                    @subject = '{Subject.Trim()}',
	                                                    @body = '{Body.Trim()}',
	                                                    @body_format = 'HTML';", sqlConnection);

                sqlCommand.ExecuteNonQuery();

                sqlCommand.Dispose();

                sqlConnection.Close();
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        /// <summary>
        /// Checks if there are accounts that are used on SiteLink but are not used on the General Ledger.
        /// </summary>
        /// <param name="locTbl">The <see cref="DataTable"/> object of the entire journal entries from each property from SiteLink.</param>
        /// <returns>Returns true if there are any accounts found at any property. It returns false if there are no properties.</returns>
        private bool HasMissingAccounts(DataTable locTbl)
        {
            try
            {
                //the outline of the email with content place holder where the {0} is
                string html = @"<html> 
                                <body>
                                    <h3>Please correct these accounts in SiteLink.</h3>
                                        <h5>These accounts do not match any General Ledger accounts.</h5>
                                            <ul>
                                                {0}    
                                            </ul>
                                </body>
                            </html>";
                //holds the individual properties with a place holder for each account at the property that needs to fixed
                string propItems = "";
                //holds the line items for each account that needs to be fixed
                string lineItems = "";

                //the email
                string body = "";

                //for storing all current General Ledger accounts
                DataSet dtAccounts = new DataSet();

                strSQL = "SELECT ACCTNUM FROM GACC";

                //gets the General Ledger accounts
                SqlDataAdapter da = new SqlDataAdapter(strSQL, Properties.Settings.Default.connMSSQL);
                da.Fill(dtAccounts);
                da.Dispose();

                //holds the current know accounts that need to be fixed for each property
                List<string> accountsFound = new List<string>();
                string prop = "";
                //goes through each line item from SiteLink
                foreach (DataRow dr in locTbl.Rows)
                {
                    //check if the account is in the General Ledger
                    if (dtAccounts.Tables[0].Select("ACCTNUM = '" + "HS" + dr["ACCTCODE"].ToString().Trim().PadRight(12, '0') + "'").Length == 0)
                    {
                        //gets the property id from the SiteLink SiteID
                        string realProp = properties.Where(x => x.SiteId == int.Parse(dr["SITEID"].ToString().Trim())).Single().PropNo.ToString();
                        //when the property id changes
                        if (lineItems != "" & realProp != prop)
                        {
                            //set the current property id
                            prop = realProp;

                            //create the property line item and set all the accounts that need to be fixed
                            propItems += string.Format(@"<li> 
                                                    {0}
                                                        <ul>
                                                           {1}
                                                         </ul>
                                                  </li>", prop, lineItems);

                            //reset the line items holding variable
                            lineItems = "";
                            //clears the accounts that were found for the property
                            accountsFound.Clear();
                        }

                        //if the account on SiteLink is not in the General Ledger accounts
                        if (!accountsFound.Contains(dr["ACCTCODE"].ToString().Trim()))
                        {
                            if (decimal.Parse(dr["Debit"].ToString().Trim()) > 0M | decimal.Parse(dr["Credit"].ToString().Trim()) > 0M)
                            {
                                //add to the accounts found list
                                accountsFound.Add(dr["ACCTCODE"].ToString().Trim());
                                //adds to the line items holding variable
                                lineItems += string.Format("<li>{0}</li>", dr["ACCTCODE"].ToString().Trim() + "-" + dr["Description"].ToString().Trim() + " " +
                                                                           dr["AcctName"].ToString().Trim() + " " + dr["SubAcctName"].ToString().Trim());
                            }
                        }
                    };
                }

                //after the each line is searched through, create the email
                body = string.Format(html, propItems);

                //reset the variables
                dtAccounts = null;
                accountsFound = null;

                //if there are properties with account that need to be fixed
                if (propItems != "")
                {
                    //send email to account and helpdesk to indicate which properties have account and the accounts that need to be fixed on SiteLink
                    SendEmail(body, "SiteLink account(s) that need to be fixed.");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        protected override void OnStart(string[] args)
        {
            Thread.Sleep(30000);

            if(args.Length > 1)
            {
                if(args[0].Trim() == "ForNonReceipts")
                {
                    if(args[1].Trim().ToUpper() == "TRUE")
                    {
                        Properties.Settings.Default.ForNonReceipts = true;
                    }
                    else if(args[1].Trim().ToUpper() == "FALSE")
                    {
                        Properties.Settings.Default.ForNonReceipts = false;
                    }
                    else
                    {
                        LogMessage($"Start Parameter 'ForNonReceipts' was not set to true/false." +
                                   $"Using existing value of {Properties.Settings.Default.ForNonReceipts}.", EventLogEntryType.Information);
                    }
                }
                else
                {
                    LogMessage($"Start Parameter 'ForNonReceipts' was not set as a start parameter." +
                               $"Using existing value of {Properties.Settings.Default.ForNonReceipts}.", EventLogEntryType.Information);
                }
            }
            else
            {
                LogMessage($"Start Parameters were not provided." +
                           $"Using existing value of {Properties.Settings.Default.ForNonReceipts} for Start Parameter 'ForNonReceipts'.", EventLogEntryType.Information);
            }

            Properties.Settings.Default.Save();

            if (Properties.Settings.Default.ForNonReceipts)
            {
                timer.Interval = 10000;

                timer.Elapsed += ElapsedTime;

                timer.Start();
            }
            else
            {
                timerReceipts.Interval = 1800000;

                timerReceipts.Elapsed += ElapsedTimeReceipts;

                timerReceipts.Start();
            }
        }

        protected override void OnStop()
        {
            timer.Stop();
            timer.Dispose();

            //if the reporting API was not instantiated, do not dispose
            if (reportingAPI == null)
            {
                LogMessage("Object reportingAPI was never instantiated. It cannot be disposed.", EventLogEntryType.Information);
            }
            else
            {
                reportingAPI.Dispose();
            }

            //if the transaction API was not instantiated, do not dispose
            if (transactionAPI == null)
            {
                LogMessage("Object transactionAPI was never instantiated. It cannot be disposed.", EventLogEntryType.Information);
            }
            else
            {
                transactionAPI.Dispose();
            }
        }
    }
}
