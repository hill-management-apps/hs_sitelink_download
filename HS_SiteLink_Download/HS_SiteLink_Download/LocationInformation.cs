﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HS_SiteLink_Download
{
    class LocationInformation
    {
        private Property _property;
        private DataSet _receipts = new DataSet();
        private DataSet _incomeAnalysis = new DataSet();
        private DataSet _generalJournal = new DataSet();
        private DataSet _managementSummary = new DataSet();

        /// <summary>
        /// Creates an empty <see cref="LocationInformation"/> object.
        /// </summary>
        public LocationInformation()
        {
            
        }

        /// <summary>
        /// Creates a <see cref="LocationInformation"/> object with an assigned <see cref="Property"/>, Receipts, Income Analysis, General Journal, and Management Summary.
        /// </summary>
        /// <param name="property">The <see cref="Property"/> information that is tied to this location.</param>
        /// <param name="receipts">The Receipts data set.</param>
        /// <param name="incomeAnalysis">The Income Analysis data set.</param>
        /// <param name="generalJournal">The General Journal data set.</param>
        /// <param name="managementSummary">The Management Summary data set.</param>
        public LocationInformation(Property property, DataSet receipts, DataSet incomeAnalysis, DataSet generalJournal, DataSet managementSummary)
        {
            _property = property;
            _receipts = receipts;
            _incomeAnalysis = incomeAnalysis;
            _generalJournal = generalJournal;
            _managementSummary = managementSummary;
        }

        /// <summary>
        /// Creates a <see cref="LocationInformation"/> object with an assigned <see cref="Property"/>, Receipts, Income Analysis, General Journal, and Management Summary.
        /// </summary>
        /// <param name="property">The <see cref="Property"/> information that is tied to this location.</param>
        /// <param name="receipts">The Receipts data set.</param>
        public LocationInformation(Property property, DataSet receipts)
        {
            _property = property;
            _receipts = receipts;
            _incomeAnalysis = null;
            _generalJournal = null;
            _managementSummary = null;
        }

        /// <summary>
        /// The <see cref="Property"/> that represents this location and the related information.
        /// </summary>
        public Property Property { get => _property; }
        /// <summary>
        /// The <see cref="DataSet"/> of the Receipts.
        /// </summary>
        public DataSet Receipts { get => _receipts; }
        /// <summary>
        /// The <see cref="DataSet"/> of Income Analysis related information.
        /// </summary>
        public DataSet IncomeAnalysis { get => _incomeAnalysis; }
        /// <summary>
        /// The <see cref="DataSet"/> of General Journal entries.
        /// </summary>
        public DataSet GeneralJournal { get => _generalJournal; }
        /// <summary>
        /// The <see cref="DataSet"/> of the Management Summary.
        /// </summary>
        public DataSet ManagementSummary { get => _managementSummary; }
    }
}
