﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HS_SiteLink_Download
{
    class Property
    {
        private int _propNo = 0;
        private string _corpCode = "";
        private string _Name = "";
        private bool _LockBoxOnly = false;
        private int _SiteId = 0;

        /// <summary>
        /// Creates a <see cref="Property"/> object with the Property Number, Corporate Code, Name, Site ID, and indcation that it is Lockbox only.
        /// </summary>
        /// <param name="propNo">The Property Number.</param>
        /// <param name="corpCode">The Corporate Code.</param>
        /// <param name="name">The Name.</param>
        /// <param name="lockBoxOnly">Indicate that is a Lockbox only or not.</param>
        /// <param name="siteId">The Site ID.</param>
        public Property(int propNo, string corpCode, string name, bool lockBoxOnly, int siteId)
        {
            _propNo = propNo;
            _corpCode = corpCode;
            _Name = name;
            _LockBoxOnly = lockBoxOnly;
            _SiteId = siteId;
        }
        
        /// <summary>
        /// The Property Number.
        /// </summary>
        public int PropNo { get => _propNo; }
        /// <summary>
        /// The Corporate Code.
        /// </summary>
        public string CorpCode { get => _corpCode; }
        /// <summary>
        /// The Name.
        /// </summary>
        public string Name { get => _Name; }
        /// <summary>
        /// Indicator that the property is lockbox only or not.
        /// </summary>
        public bool LockBoxOnly { get => _LockBoxOnly; }
        /// <summary>
        /// The Site ID.
        /// </summary>
        public int SiteId { get => _SiteId; }
    }
}
